package ro.usv.rf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class DistanceUtils {

	public static java.util.List<Map<String, Double>> procents = new ArrayList<>();

	public static double  calculateEuclidianDistance(double p1[],String p2[],int noOfF) {

		double suma=0.0; 
		for(int i=0;i<noOfF;i++) {
			double r=p1[i]-Double.valueOf(p2[i]);
			r=Math.pow(r,2);
			suma+=r;
		}

		return Math.round(Math.sqrt(suma)*100.0)/100.0;
	}

	public static TreeMap<Double, String> calculateEuclidianDistanceMatrice(
			String [][]learningSet, double[] city) {
		// Distances
		TreeMap<Double, String> treeMap = new TreeMap<Double, String>();
		// Number of ocurences
		HashMap<String, Double> percentages = new HashMap<>();

		double  [][]mat=new double [learningSet.length][2];
		for(int i=0;i<learningSet.length;i++)
		{
			for(int j=0;j<1;j++)
			{
				mat[i][j] = calculateEuclidianDistance(city,learningSet[i], 2);
				treeMap.put(mat[i][j], learningSet[i][3]);

				if(percentages.get(learningSet[i][3]) == null) {
					percentages.put(learningSet[i][3], (double) 1);
				} else {
					percentages.put(learningSet[i][3], 
							percentages.get(learningSet[i][3]) + 1);
				}
			}
		}

		procents.add(percentages);
		return treeMap;
	}
}
