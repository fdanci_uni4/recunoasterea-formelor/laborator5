package ro.usv.rf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * 
 * @author fdanci
 *
 */
public class MainClass {

	public static void main(String[] args) {
		String [][] learningSet;

		try {
			learningSet = FileUtils.readLearningSetFromFile("data.csv");
			PrintWriter pw = null;
			pw = new PrintWriter(new File("out.txt"));

			TreeMap<Double, String> treeMap = 
					new TreeMap<Double, String>();

			int numberOfPatterns = learningSet.length;
			int numberOfFeatures = learningSet[0].length;

			int[] ocurences = {31, 31, 31};

			double[][] location = new double[][] { 
				{25.89,47.56},
				{24.0,45.15},
				{25.33,45.44} };

				for(int i = 0; i < location.length; i++) {
					System.out.println("-----------------Location " + (i + 1) + " -----------------");
					pw.println("-----------------Location " + (i + 1) + " -----------------");
					
					treeMap = DistanceUtils.calculateEuclidianDistanceMatrice(learningSet, location[i]);
					
					int count = ocurences[i];
					HashMap<String, Double> percentages = new HashMap<>();

					// Display Distances
					for (Map.Entry<Double,String> entry : treeMap.entrySet()) {
						String value = entry.getValue();
						Double key = entry.getKey();
						System.out.print(value + " -> ");
						System.out.println(key);
						pw.print(value + " -> ");
						pw.println(key);

						if(percentages.get(value) == null) {
							percentages.put(value, (double) 1);
						} else {
							percentages.put(value, percentages.get(value) + 1);
						}

						if(--count == 0) {
							break;
						}
					}

					// Display Percentage
					for (Map.Entry<String,Double> entry : percentages.entrySet()) {
						Double value = entry.getValue();
						String key = entry.getKey();

						DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
						System.out.println(key + " appears  " + value + " times" + "(" + 
								decimalFormat.format(value /  ocurences[i] * 100)  + "%)");
						pw.println(key + " appears  " + value + " times" + "(" + 
								decimalFormat.format(value /  ocurences[i] * 100)  + "%)");
					}
					
					System.out.println();
					pw.println();
				}

				System.out.println(String.format("\nThe learning set has "
						+ "%s patters and %s features", numberOfPatterns, numberOfFeatures));
				pw.println(String.format("\nThe learning set has "
						+ "%s patters and %s features", numberOfPatterns, numberOfFeatures));

				if(pw != null) {
					pw.close();
				}

		} catch (USVInputFileCustomException ex) {
			System.out.println(ex.getMessage());
		} catch (FileNotFoundException ex) {
			System.out.println(ex.getMessage());
		} finally {
			System.out.println("Finished learning set operations");
		}
	}

}
